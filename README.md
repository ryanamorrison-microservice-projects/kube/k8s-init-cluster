k8s-init-cluster
=========

Initializes the prepared kubernetes cluster and installs a CNI. This series of roles is more purpose-specific than a general implementation like kube-spray.

Requirements
------------
Assumes certain ansible inventory groups are present in a specific hierarchy:
```
---
firstcp:
  hosts:
    cp1:
      var_host_endpoint_ip: 192.168.1.2
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
restcps:
  hosts:
    cp2:
      var_host_endpoint_ip: 192.168.1.3
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
allcps:
  children:
    firstcp:
    restcps:
  vars:
    var_kube_version: 1.27
    var_kube_install_version: 1.27.1-00
    var_service_subnet_cidr: 10.244.0.0/16
    var_pod_subnet_cidr: 172.20.0.0/16
    var_node_mask_cidr: 24
    var_dns_domain: cluster.local
    var_cluster_name: kubernetes
    var_storage: rook
    var_cni: cilium
    var_cilium_install_with_helm: false
    var_cilium_use_gw_api: false
    var_cilium_use_kube_proxy: true
    cilium_version: 1.14.6
```
Also see variables below

Role Variables
--------------
Are broken into two categories based on where they are located.
### default variables
Variable default values listed below.  Any/most/all of these should be in an inventory file.  Sane defaults provided for testing.     
   
Which networking overlay to use, currently supported are Flannel and Cilium.  
```
var_cni: cilium
```
If using Cilium, whether to use Kube's proxy or Cilium's proxy.  Some service meshes expect kube-proxy but Cilium's allows for finer-grained network policies.
```
var_cilium_use_kube_proxy: true
```
If using Cilium, whether to use GatewayAPI (future of ingress).
```
var_cilium_use_gw_api: false
```
If using Cilium, set this to true to use an upstream BGP router/L3 switch, etc.  Note: enabling this allows for the creation of Cilium BGP policy routing resources in k8s.  Peering information such as ASN's are set in those resources.
```
var_cilium_bgp_control_plane: false
```
Set to true if Hubble UI (Cilium) should be installed in the cluster.
```
var_cilium_hubble_ui_enabled: false
```
The domain name CoreDNS will use for service discovery.
```
var_dns_domain: cluster.local
```
The pod subnet with CIDR mask.  This is purely for pod-to-pod communication and the overlay will typically encapsulate this over something like VXLAN so that it is transparent to the network switches the nodes are directly connected to.
```
var_pod_subnet_cidr: "172.20.0.0/16"
```
The service subnet with CIDR mask.  This is the IP range service addresses are attached to and what the external load balancer (e.g. an ALB) and/or ingress controller will send traffic to.
```
var_service_subnet_cidr: "10.244.0.0/16"
```
This node mask is how large to slice up the network ranges as they are assigned to nodes.  Each node gets a non-overlapping range.  This obviously has to be smaller than the subnet CIDR above.  Using the above example and the default value, controller-1 would get 172.20.1.0/24, controller-2 would get 172.20.2.0/24, etc.
```
var_node_mask_cidr: 24
```
The name of the cluster:
```
var_cluster_name: kubernetes
```


### inventory variables
each node (ec2 instance, host, QEMU vm, etc.) is assumed to have a ssh key that CI runners will use to connect to the host:
```
ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
```

Dependencies
------------
* k8s-common
* k8s-cps-common

Also see Playbook below    

Example Playbook
----------------
```
---
- hosts: allkube
  become: true
  become_method: sudo
  roles:
    - k8s-common

- hosts: allcps
  become: true
  become_method: sudo
  roles:
    - k8s-cps-common

- hosts: firstcp
  become: true
  become_method: sudo
  roles:
    - k8s-init-cluster
```

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
